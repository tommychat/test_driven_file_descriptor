-------- day 0 --------
name, sellIn, quality
Item { name: '+5 Dexterity Vest', sellIn: 10, quality: 20 }
Item { name: 'Aged Brie', sellIn: 2, quality: 0 }
Item { name: 'Elixir of the Mongoose', sellIn: 5, quality: 7 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: 0, quality: 80 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: -1, quality: 80 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: 15,
  quality: 20 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: 10,
  quality: 49 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: 5,
  quality: 49 }
Item { name: 'Conjured Mana Cake', sellIn: 3, quality: 6 }

-------- day 1 --------
name, sellIn, quality
Item { name: '+5 Dexterity Vest', sellIn: 9, quality: 19 }
Item { name: 'Aged Brie', sellIn: 1, quality: 1 }
Item { name: 'Elixir of the Mongoose', sellIn: 4, quality: 6 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: 0, quality: 80 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: -1, quality: 80 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: 14,
  quality: 21 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: 9,
  quality: 50 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: 4,
  quality: 50 }
Item { name: 'Conjured Mana Cake', sellIn: 2, quality: 5 }

-------- day 2 --------
name, sellIn, quality
Item { name: '+5 Dexterity Vest', sellIn: 8, quality: 18 }
Item { name: 'Aged Brie', sellIn: 0, quality: 2 }
Item { name: 'Elixir of the Mongoose', sellIn: 3, quality: 5 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: 0, quality: 80 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: -1, quality: 80 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: 13,
  quality: 22 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: 8,
  quality: 50 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: 3,
  quality: 50 }
Item { name: 'Conjured Mana Cake', sellIn: 1, quality: 4 }

-------- day 3 --------
name, sellIn, quality
Item { name: '+5 Dexterity Vest', sellIn: 7, quality: 17 }
Item { name: 'Aged Brie', sellIn: -1, quality: 4 }
Item { name: 'Elixir of the Mongoose', sellIn: 2, quality: 4 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: 0, quality: 80 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: -1, quality: 80 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: 12,
  quality: 23 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: 7,
  quality: 50 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: 2,
  quality: 50 }
Item { name: 'Conjured Mana Cake', sellIn: 0, quality: 3 }

-------- day 4 --------
name, sellIn, quality
Item { name: '+5 Dexterity Vest', sellIn: 6, quality: 16 }
Item { name: 'Aged Brie', sellIn: -2, quality: 6 }
Item { name: 'Elixir of the Mongoose', sellIn: 1, quality: 3 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: 0, quality: 80 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: -1, quality: 80 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: 11,
  quality: 24 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: 6,
  quality: 50 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: 1,
  quality: 50 }
Item { name: 'Conjured Mana Cake', sellIn: -1, quality: 1 }

-------- day 5 --------
name, sellIn, quality
Item { name: '+5 Dexterity Vest', sellIn: 5, quality: 15 }
Item { name: 'Aged Brie', sellIn: -3, quality: 8 }
Item { name: 'Elixir of the Mongoose', sellIn: 0, quality: 2 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: 0, quality: 80 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: -1, quality: 80 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: 10,
  quality: 25 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: 5,
  quality: 50 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: 0,
  quality: 50 }
Item { name: 'Conjured Mana Cake', sellIn: -2, quality: 0 }

-------- day 6 --------
name, sellIn, quality
Item { name: '+5 Dexterity Vest', sellIn: 4, quality: 14 }
Item { name: 'Aged Brie', sellIn: -4, quality: 10 }
Item { name: 'Elixir of the Mongoose', sellIn: -1, quality: 0 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: 0, quality: 80 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: -1, quality: 80 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: 9,
  quality: 27 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: 4,
  quality: 50 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -1,
  quality: 0 }
Item { name: 'Conjured Mana Cake', sellIn: -3, quality: 0 }

-------- day 7 --------
name, sellIn, quality
Item { name: '+5 Dexterity Vest', sellIn: 3, quality: 13 }
Item { name: 'Aged Brie', sellIn: -5, quality: 12 }
Item { name: 'Elixir of the Mongoose', sellIn: -2, quality: 0 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: 0, quality: 80 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: -1, quality: 80 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: 8,
  quality: 29 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: 3,
  quality: 50 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -2,
  quality: 0 }
Item { name: 'Conjured Mana Cake', sellIn: -4, quality: 0 }

-------- day 8 --------
name, sellIn, quality
Item { name: '+5 Dexterity Vest', sellIn: 2, quality: 12 }
Item { name: 'Aged Brie', sellIn: -6, quality: 14 }
Item { name: 'Elixir of the Mongoose', sellIn: -3, quality: 0 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: 0, quality: 80 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: -1, quality: 80 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: 7,
  quality: 31 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: 2,
  quality: 50 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -3,
  quality: 0 }
Item { name: 'Conjured Mana Cake', sellIn: -5, quality: 0 }

-------- day 9 --------
name, sellIn, quality
Item { name: '+5 Dexterity Vest', sellIn: 1, quality: 11 }
Item { name: 'Aged Brie', sellIn: -7, quality: 16 }
Item { name: 'Elixir of the Mongoose', sellIn: -4, quality: 0 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: 0, quality: 80 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: -1, quality: 80 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: 6,
  quality: 33 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: 1,
  quality: 50 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -4,
  quality: 0 }
Item { name: 'Conjured Mana Cake', sellIn: -6, quality: 0 }

-------- day 10 --------
name, sellIn, quality
Item { name: '+5 Dexterity Vest', sellIn: 0, quality: 10 }
Item { name: 'Aged Brie', sellIn: -8, quality: 18 }
Item { name: 'Elixir of the Mongoose', sellIn: -5, quality: 0 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: 0, quality: 80 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: -1, quality: 80 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: 5,
  quality: 35 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: 0,
  quality: 50 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -5,
  quality: 0 }
Item { name: 'Conjured Mana Cake', sellIn: -7, quality: 0 }

-------- day 11 --------
name, sellIn, quality
Item { name: '+5 Dexterity Vest', sellIn: -1, quality: 8 }
Item { name: 'Aged Brie', sellIn: -9, quality: 20 }
Item { name: 'Elixir of the Mongoose', sellIn: -6, quality: 0 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: 0, quality: 80 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: -1, quality: 80 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: 4,
  quality: 38 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -1,
  quality: 0 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -6,
  quality: 0 }
Item { name: 'Conjured Mana Cake', sellIn: -8, quality: 0 }

-------- day 12 --------
name, sellIn, quality
Item { name: '+5 Dexterity Vest', sellIn: -2, quality: 6 }
Item { name: 'Aged Brie', sellIn: -10, quality: 22 }
Item { name: 'Elixir of the Mongoose', sellIn: -7, quality: 0 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: 0, quality: 80 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: -1, quality: 80 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: 3,
  quality: 41 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -2,
  quality: 0 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -7,
  quality: 0 }
Item { name: 'Conjured Mana Cake', sellIn: -9, quality: 0 }

-------- day 13 --------
name, sellIn, quality
Item { name: '+5 Dexterity Vest', sellIn: -3, quality: 4 }
Item { name: 'Aged Brie', sellIn: -11, quality: 24 }
Item { name: 'Elixir of the Mongoose', sellIn: -8, quality: 0 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: 0, quality: 80 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: -1, quality: 80 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: 2,
  quality: 44 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -3,
  quality: 0 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -8,
  quality: 0 }
Item { name: 'Conjured Mana Cake', sellIn: -10, quality: 0 }

-------- day 14 --------
name, sellIn, quality
Item { name: '+5 Dexterity Vest', sellIn: -4, quality: 2 }
Item { name: 'Aged Brie', sellIn: -12, quality: 26 }
Item { name: 'Elixir of the Mongoose', sellIn: -9, quality: 0 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: 0, quality: 80 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: -1, quality: 80 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: 1,
  quality: 47 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -4,
  quality: 0 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -9,
  quality: 0 }
Item { name: 'Conjured Mana Cake', sellIn: -11, quality: 0 }

-------- day 15 --------
name, sellIn, quality
Item { name: '+5 Dexterity Vest', sellIn: -5, quality: 0 }
Item { name: 'Aged Brie', sellIn: -13, quality: 28 }
Item { name: 'Elixir of the Mongoose', sellIn: -10, quality: 0 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: 0, quality: 80 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: -1, quality: 80 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: 0,
  quality: 50 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -5,
  quality: 0 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -10,
  quality: 0 }
Item { name: 'Conjured Mana Cake', sellIn: -12, quality: 0 }

-------- day 16 --------
name, sellIn, quality
Item { name: '+5 Dexterity Vest', sellIn: -6, quality: 0 }
Item { name: 'Aged Brie', sellIn: -14, quality: 30 }
Item { name: 'Elixir of the Mongoose', sellIn: -11, quality: 0 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: 0, quality: 80 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: -1, quality: 80 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -1,
  quality: 0 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -6,
  quality: 0 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -11,
  quality: 0 }
Item { name: 'Conjured Mana Cake', sellIn: -13, quality: 0 }

-------- day 17 --------
name, sellIn, quality
Item { name: '+5 Dexterity Vest', sellIn: -7, quality: 0 }
Item { name: 'Aged Brie', sellIn: -15, quality: 32 }
Item { name: 'Elixir of the Mongoose', sellIn: -12, quality: 0 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: 0, quality: 80 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: -1, quality: 80 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -2,
  quality: 0 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -7,
  quality: 0 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -12,
  quality: 0 }
Item { name: 'Conjured Mana Cake', sellIn: -14, quality: 0 }

-------- day 18 --------
name, sellIn, quality
Item { name: '+5 Dexterity Vest', sellIn: -8, quality: 0 }
Item { name: 'Aged Brie', sellIn: -16, quality: 34 }
Item { name: 'Elixir of the Mongoose', sellIn: -13, quality: 0 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: 0, quality: 80 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: -1, quality: 80 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -3,
  quality: 0 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -8,
  quality: 0 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -13,
  quality: 0 }
Item { name: 'Conjured Mana Cake', sellIn: -15, quality: 0 }

-------- day 19 --------
name, sellIn, quality
Item { name: '+5 Dexterity Vest', sellIn: -9, quality: 0 }
Item { name: 'Aged Brie', sellIn: -17, quality: 36 }
Item { name: 'Elixir of the Mongoose', sellIn: -14, quality: 0 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: 0, quality: 80 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: -1, quality: 80 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -4,
  quality: 0 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -9,
  quality: 0 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -14,
  quality: 0 }
Item { name: 'Conjured Mana Cake', sellIn: -16, quality: 0 }

-------- day 20 --------
name, sellIn, quality
Item { name: '+5 Dexterity Vest', sellIn: -10, quality: 0 }
Item { name: 'Aged Brie', sellIn: -18, quality: 38 }
Item { name: 'Elixir of the Mongoose', sellIn: -15, quality: 0 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: 0, quality: 80 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: -1, quality: 80 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -5,
  quality: 0 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -10,
  quality: 0 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -15,
  quality: 0 }
Item { name: 'Conjured Mana Cake', sellIn: -17, quality: 0 }

-------- day 21 --------
name, sellIn, quality
Item { name: '+5 Dexterity Vest', sellIn: -11, quality: 0 }
Item { name: 'Aged Brie', sellIn: -19, quality: 40 }
Item { name: 'Elixir of the Mongoose', sellIn: -16, quality: 0 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: 0, quality: 80 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: -1, quality: 80 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -6,
  quality: 0 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -11,
  quality: 0 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -16,
  quality: 0 }
Item { name: 'Conjured Mana Cake', sellIn: -18, quality: 0 }

-------- day 22 --------
name, sellIn, quality
Item { name: '+5 Dexterity Vest', sellIn: -12, quality: 0 }
Item { name: 'Aged Brie', sellIn: -20, quality: 42 }
Item { name: 'Elixir of the Mongoose', sellIn: -17, quality: 0 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: 0, quality: 80 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: -1, quality: 80 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -7,
  quality: 0 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -12,
  quality: 0 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -17,
  quality: 0 }
Item { name: 'Conjured Mana Cake', sellIn: -19, quality: 0 }

-------- day 23 --------
name, sellIn, quality
Item { name: '+5 Dexterity Vest', sellIn: -13, quality: 0 }
Item { name: 'Aged Brie', sellIn: -21, quality: 44 }
Item { name: 'Elixir of the Mongoose', sellIn: -18, quality: 0 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: 0, quality: 80 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: -1, quality: 80 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -8,
  quality: 0 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -13,
  quality: 0 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -18,
  quality: 0 }
Item { name: 'Conjured Mana Cake', sellIn: -20, quality: 0 }

-------- day 24 --------
name, sellIn, quality
Item { name: '+5 Dexterity Vest', sellIn: -14, quality: 0 }
Item { name: 'Aged Brie', sellIn: -22, quality: 46 }
Item { name: 'Elixir of the Mongoose', sellIn: -19, quality: 0 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: 0, quality: 80 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: -1, quality: 80 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -9,
  quality: 0 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -14,
  quality: 0 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -19,
  quality: 0 }
Item { name: 'Conjured Mana Cake', sellIn: -21, quality: 0 }

-------- day 25 --------
name, sellIn, quality
Item { name: '+5 Dexterity Vest', sellIn: -15, quality: 0 }
Item { name: 'Aged Brie', sellIn: -23, quality: 48 }
Item { name: 'Elixir of the Mongoose', sellIn: -20, quality: 0 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: 0, quality: 80 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: -1, quality: 80 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -10,
  quality: 0 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -15,
  quality: 0 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -20,
  quality: 0 }
Item { name: 'Conjured Mana Cake', sellIn: -22, quality: 0 }

-------- day 26 --------
name, sellIn, quality
Item { name: '+5 Dexterity Vest', sellIn: -16, quality: 0 }
Item { name: 'Aged Brie', sellIn: -24, quality: 50 }
Item { name: 'Elixir of the Mongoose', sellIn: -21, quality: 0 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: 0, quality: 80 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: -1, quality: 80 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -11,
  quality: 0 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -16,
  quality: 0 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -21,
  quality: 0 }
Item { name: 'Conjured Mana Cake', sellIn: -23, quality: 0 }

-------- day 27 --------
name, sellIn, quality
Item { name: '+5 Dexterity Vest', sellIn: -17, quality: 0 }
Item { name: 'Aged Brie', sellIn: -25, quality: 50 }
Item { name: 'Elixir of the Mongoose', sellIn: -22, quality: 0 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: 0, quality: 80 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: -1, quality: 80 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -12,
  quality: 0 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -17,
  quality: 0 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -22,
  quality: 0 }
Item { name: 'Conjured Mana Cake', sellIn: -24, quality: 0 }

-------- day 28 --------
name, sellIn, quality
Item { name: '+5 Dexterity Vest', sellIn: -18, quality: 0 }
Item { name: 'Aged Brie', sellIn: -26, quality: 50 }
Item { name: 'Elixir of the Mongoose', sellIn: -23, quality: 0 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: 0, quality: 80 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: -1, quality: 80 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -13,
  quality: 0 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -18,
  quality: 0 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -23,
  quality: 0 }
Item { name: 'Conjured Mana Cake', sellIn: -25, quality: 0 }

-------- day 29 --------
name, sellIn, quality
Item { name: '+5 Dexterity Vest', sellIn: -19, quality: 0 }
Item { name: 'Aged Brie', sellIn: -27, quality: 50 }
Item { name: 'Elixir of the Mongoose', sellIn: -24, quality: 0 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: 0, quality: 80 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: -1, quality: 80 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -14,
  quality: 0 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -19,
  quality: 0 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -24,
  quality: 0 }
Item { name: 'Conjured Mana Cake', sellIn: -26, quality: 0 }

-------- day 30 --------
name, sellIn, quality
Item { name: '+5 Dexterity Vest', sellIn: -20, quality: 0 }
Item { name: 'Aged Brie', sellIn: -28, quality: 50 }
Item { name: 'Elixir of the Mongoose', sellIn: -25, quality: 0 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: 0, quality: 80 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: -1, quality: 80 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -15,
  quality: 0 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -20,
  quality: 0 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -25,
  quality: 0 }
Item { name: 'Conjured Mana Cake', sellIn: -27, quality: 0 }

-------- day 31 --------
name, sellIn, quality
Item { name: '+5 Dexterity Vest', sellIn: -21, quality: 0 }
Item { name: 'Aged Brie', sellIn: -29, quality: 50 }
Item { name: 'Elixir of the Mongoose', sellIn: -26, quality: 0 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: 0, quality: 80 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: -1, quality: 80 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -16,
  quality: 0 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -21,
  quality: 0 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -26,
  quality: 0 }
Item { name: 'Conjured Mana Cake', sellIn: -28, quality: 0 }

-------- day 32 --------
name, sellIn, quality
Item { name: '+5 Dexterity Vest', sellIn: -22, quality: 0 }
Item { name: 'Aged Brie', sellIn: -30, quality: 50 }
Item { name: 'Elixir of the Mongoose', sellIn: -27, quality: 0 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: 0, quality: 80 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: -1, quality: 80 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -17,
  quality: 0 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -22,
  quality: 0 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -27,
  quality: 0 }
Item { name: 'Conjured Mana Cake', sellIn: -29, quality: 0 }

-------- day 33 --------
name, sellIn, quality
Item { name: '+5 Dexterity Vest', sellIn: -23, quality: 0 }
Item { name: 'Aged Brie', sellIn: -31, quality: 50 }
Item { name: 'Elixir of the Mongoose', sellIn: -28, quality: 0 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: 0, quality: 80 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: -1, quality: 80 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -18,
  quality: 0 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -23,
  quality: 0 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -28,
  quality: 0 }
Item { name: 'Conjured Mana Cake', sellIn: -30, quality: 0 }

-------- day 34 --------
name, sellIn, quality
Item { name: '+5 Dexterity Vest', sellIn: -24, quality: 0 }
Item { name: 'Aged Brie', sellIn: -32, quality: 50 }
Item { name: 'Elixir of the Mongoose', sellIn: -29, quality: 0 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: 0, quality: 80 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: -1, quality: 80 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -19,
  quality: 0 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -24,
  quality: 0 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -29,
  quality: 0 }
Item { name: 'Conjured Mana Cake', sellIn: -31, quality: 0 }

-------- day 35 --------
name, sellIn, quality
Item { name: '+5 Dexterity Vest', sellIn: -25, quality: 0 }
Item { name: 'Aged Brie', sellIn: -33, quality: 50 }
Item { name: 'Elixir of the Mongoose', sellIn: -30, quality: 0 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: 0, quality: 80 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: -1, quality: 80 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -20,
  quality: 0 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -25,
  quality: 0 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -30,
  quality: 0 }
Item { name: 'Conjured Mana Cake', sellIn: -32, quality: 0 }

-------- day 36 --------
name, sellIn, quality
Item { name: '+5 Dexterity Vest', sellIn: -26, quality: 0 }
Item { name: 'Aged Brie', sellIn: -34, quality: 50 }
Item { name: 'Elixir of the Mongoose', sellIn: -31, quality: 0 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: 0, quality: 80 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: -1, quality: 80 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -21,
  quality: 0 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -26,
  quality: 0 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -31,
  quality: 0 }
Item { name: 'Conjured Mana Cake', sellIn: -33, quality: 0 }

-------- day 37 --------
name, sellIn, quality
Item { name: '+5 Dexterity Vest', sellIn: -27, quality: 0 }
Item { name: 'Aged Brie', sellIn: -35, quality: 50 }
Item { name: 'Elixir of the Mongoose', sellIn: -32, quality: 0 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: 0, quality: 80 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: -1, quality: 80 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -22,
  quality: 0 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -27,
  quality: 0 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -32,
  quality: 0 }
Item { name: 'Conjured Mana Cake', sellIn: -34, quality: 0 }

-------- day 38 --------
name, sellIn, quality
Item { name: '+5 Dexterity Vest', sellIn: -28, quality: 0 }
Item { name: 'Aged Brie', sellIn: -36, quality: 50 }
Item { name: 'Elixir of the Mongoose', sellIn: -33, quality: 0 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: 0, quality: 80 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: -1, quality: 80 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -23,
  quality: 0 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -28,
  quality: 0 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -33,
  quality: 0 }
Item { name: 'Conjured Mana Cake', sellIn: -35, quality: 0 }

-------- day 39 --------
name, sellIn, quality
Item { name: '+5 Dexterity Vest', sellIn: -29, quality: 0 }
Item { name: 'Aged Brie', sellIn: -37, quality: 50 }
Item { name: 'Elixir of the Mongoose', sellIn: -34, quality: 0 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: 0, quality: 80 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: -1, quality: 80 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -24,
  quality: 0 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -29,
  quality: 0 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -34,
  quality: 0 }
Item { name: 'Conjured Mana Cake', sellIn: -36, quality: 0 }

-------- day 40 --------
name, sellIn, quality
Item { name: '+5 Dexterity Vest', sellIn: -30, quality: 0 }
Item { name: 'Aged Brie', sellIn: -38, quality: 50 }
Item { name: 'Elixir of the Mongoose', sellIn: -35, quality: 0 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: 0, quality: 80 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: -1, quality: 80 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -25,
  quality: 0 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -30,
  quality: 0 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -35,
  quality: 0 }
Item { name: 'Conjured Mana Cake', sellIn: -37, quality: 0 }

-------- day 41 --------
name, sellIn, quality
Item { name: '+5 Dexterity Vest', sellIn: -31, quality: 0 }
Item { name: 'Aged Brie', sellIn: -39, quality: 50 }
Item { name: 'Elixir of the Mongoose', sellIn: -36, quality: 0 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: 0, quality: 80 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: -1, quality: 80 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -26,
  quality: 0 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -31,
  quality: 0 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -36,
  quality: 0 }
Item { name: 'Conjured Mana Cake', sellIn: -38, quality: 0 }

-------- day 42 --------
name, sellIn, quality
Item { name: '+5 Dexterity Vest', sellIn: -32, quality: 0 }
Item { name: 'Aged Brie', sellIn: -40, quality: 50 }
Item { name: 'Elixir of the Mongoose', sellIn: -37, quality: 0 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: 0, quality: 80 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: -1, quality: 80 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -27,
  quality: 0 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -32,
  quality: 0 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -37,
  quality: 0 }
Item { name: 'Conjured Mana Cake', sellIn: -39, quality: 0 }

-------- day 43 --------
name, sellIn, quality
Item { name: '+5 Dexterity Vest', sellIn: -33, quality: 0 }
Item { name: 'Aged Brie', sellIn: -41, quality: 50 }
Item { name: 'Elixir of the Mongoose', sellIn: -38, quality: 0 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: 0, quality: 80 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: -1, quality: 80 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -28,
  quality: 0 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -33,
  quality: 0 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -38,
  quality: 0 }
Item { name: 'Conjured Mana Cake', sellIn: -40, quality: 0 }

-------- day 44 --------
name, sellIn, quality
Item { name: '+5 Dexterity Vest', sellIn: -34, quality: 0 }
Item { name: 'Aged Brie', sellIn: -42, quality: 50 }
Item { name: 'Elixir of the Mongoose', sellIn: -39, quality: 0 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: 0, quality: 80 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: -1, quality: 80 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -29,
  quality: 0 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -34,
  quality: 0 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -39,
  quality: 0 }
Item { name: 'Conjured Mana Cake', sellIn: -41, quality: 0 }

-------- day 45 --------
name, sellIn, quality
Item { name: '+5 Dexterity Vest', sellIn: -35, quality: 0 }
Item { name: 'Aged Brie', sellIn: -43, quality: 50 }
Item { name: 'Elixir of the Mongoose', sellIn: -40, quality: 0 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: 0, quality: 80 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: -1, quality: 80 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -30,
  quality: 0 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -35,
  quality: 0 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -40,
  quality: 0 }
Item { name: 'Conjured Mana Cake', sellIn: -42, quality: 0 }

-------- day 46 --------
name, sellIn, quality
Item { name: '+5 Dexterity Vest', sellIn: -36, quality: 0 }
Item { name: 'Aged Brie', sellIn: -44, quality: 50 }
Item { name: 'Elixir of the Mongoose', sellIn: -41, quality: 0 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: 0, quality: 80 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: -1, quality: 80 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -31,
  quality: 0 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -36,
  quality: 0 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -41,
  quality: 0 }
Item { name: 'Conjured Mana Cake', sellIn: -43, quality: 0 }

-------- day 47 --------
name, sellIn, quality
Item { name: '+5 Dexterity Vest', sellIn: -37, quality: 0 }
Item { name: 'Aged Brie', sellIn: -45, quality: 50 }
Item { name: 'Elixir of the Mongoose', sellIn: -42, quality: 0 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: 0, quality: 80 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: -1, quality: 80 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -32,
  quality: 0 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -37,
  quality: 0 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -42,
  quality: 0 }
Item { name: 'Conjured Mana Cake', sellIn: -44, quality: 0 }

-------- day 48 --------
name, sellIn, quality
Item { name: '+5 Dexterity Vest', sellIn: -38, quality: 0 }
Item { name: 'Aged Brie', sellIn: -46, quality: 50 }
Item { name: 'Elixir of the Mongoose', sellIn: -43, quality: 0 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: 0, quality: 80 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: -1, quality: 80 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -33,
  quality: 0 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -38,
  quality: 0 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -43,
  quality: 0 }
Item { name: 'Conjured Mana Cake', sellIn: -45, quality: 0 }

-------- day 49 --------
name, sellIn, quality
Item { name: '+5 Dexterity Vest', sellIn: -39, quality: 0 }
Item { name: 'Aged Brie', sellIn: -47, quality: 50 }
Item { name: 'Elixir of the Mongoose', sellIn: -44, quality: 0 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: 0, quality: 80 }
Item { name: 'Sulfuras, Hand of Ragnaros', sellIn: -1, quality: 80 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -34,
  quality: 0 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -39,
  quality: 0 }
Item {
  name: 'Backstage passes to a TAFKAL80ETC concert',
  sellIn: -44,
  quality: 0 }
Item { name: 'Conjured Mana Cake', sellIn: -46, quality: 0 }

